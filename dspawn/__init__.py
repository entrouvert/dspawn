#!/usr/bin/env python
""" dspawn is a manager for systemd-nspawn containers, an alternative to
machinectl aimed at debootstrap compatible distributions, with the folowing
features: authorizes ssh keys in container, caches deboostrap models for fast
container creations, support various network configurations, uses apt-cacher-ng
if available, and more (note: for zone network setup, please systemctl enable
systemd-networkd)

Example usage: dspawn create bar.foo; ssh roo@bar.foo
"""
