#!/usr/bin/env python
from dspawn.container import Machine
import subprocess
import shlex
import time


class Manager:
    def __init__(self, *args, **kwargs):
        self.machines = []
        self.get_list()

    def get_list(self):
        ll = subprocess.check_output(shlex.split('machinectl list'))
        for l in ll.decode().splitlines():
            if 'container' in l:
                n = l.split()[0]
                m = Machine(n)
                self.machines.append(m)

    def stopall(self):
        for m in self.machines:
            print(m.name)
            m.stop()
        time.sleep(1)
        for m in self.machines:
            m.terminate()
