#!/usr/bin/env python
import os
import sys
import argparse
import dspawn
import logging
from dspawn.container import Machine
from dspawn.container import logger
from dspawn.manager import Manager

machinectl_actions = ['list', 'list-images', 'start', 'stop', 'show',
                      'shell', 'remove', 'login', 'enable', 'disable',
                      'kill', 'reboot', 'terminate', 'login']


def main():
    parser = argparse.ArgumentParser(description=dspawn.__doc__)
    parser.add_argument('action',
                        choices=['create', 'config', 'stopall']
                        + machinectl_actions,
                        help='which task shall we perform?')
    parser.add_argument('name',
                        nargs='*',
                        help='container name(s)')
    parser.add_argument('-r', '--release',
                        choices=['stable', 'jessie', 'stretch', 'buster', 'bullseye'],
                        default='stable',
                        help='which release model shall we use;'
                        'dafault: stable')
    parser.add_argument('-a', '--address', metavar="x.x.y.z",
                        help='static ip address')
    parser.add_argument('-b', '--bind', metavar="/path/to/bind",
                        help='bind mount folder or device')
    parser.add_argument('-m', '--mode',
                        default='zone',
                        choices=['private', 'zone', 'bridge'],
                        help='configure nspawn network; default: '
                        'containers cannot talk to each others')
    parser.add_argument('-c', '--macaddress',
                        help='specify container mac address'),
    parser.add_argument('-p', '--privateoff',
                        action='store_true',
                        help='turn off PrivateUsers mapping'),
    parser.add_argument('-e', '--enable',
                        action='store_true',
                        help='enable service (append it to machines.target.wants)'),
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='display debug messages'),

    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    if args.address and args.mode == 'private':
        logger.warning('private networking mode not supported with static address')
        args.mode = 'zone'

    if args.action == 'create' or args.action == 'config':
        if not args.name:
            parser.print_help()
            sys.exit()
        m = Machine(**vars(args))
        getattr(m, args.action)()

    if args.action in machinectl_actions:
        if args.name:
            ar = ' '.join(args.name)
        else:
            ar = ''
        os.system('machinectl %s %s' % (args.action, ar))

    if args.action == 'stopall':
        mgr = Manager(**vars(args))
        mgr.stopall()

if __name__ == '__main__':
    main()
