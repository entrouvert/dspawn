import os
from setuptools import setup
import subprocess


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--dirty=.dirty','--match=v*'],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(
                    subprocess.check_output(
                            ['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


setup(name='dspawn',
      version=get_version(),
      description='systemd-nspawn container manager',
      author='Christophe Siraut',
      author_email='csiraut@entrouvert.com',
      license='GPL',
      packages=['dspawn'],
      install_requires=[
                    'python-apt',
                ],
      entry_points = {
          'console_scripts': ['dspawn=dspawn.cli:main'],
          },
      zip_safe=False)
